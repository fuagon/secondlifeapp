package com.cnam.secondlifeapp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.model.Product;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

public class AddOfferAdapter extends RecyclerView.Adapter<AddOfferAdapter.AddOfferViewHolder> {
    private List<Product> addOfferList;

    public  AddOfferAdapter(List<Product> addOfferList) {this.addOfferList = addOfferList;}

    @NonNull
    @Override
    public AddOfferViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.addoffer_item, parent, false);
        return new AddOfferViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddOfferViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {return addOfferList.size(); }

    public static class AddOfferViewHolder extends RecyclerView.ViewHolder{
        Button btnAddOffer, btnAddPicture;
        TextInputEditText descriptionInput, priceInput;

        public AddOfferViewHolder(@NonNull View itemView) {
            super(itemView);
            btnAddPicture = itemView.findViewById(R.id.ButtonAddPicture);
            btnAddOffer = itemView.findViewById(R.id.ButtonAddOffer);
            descriptionInput = itemView.findViewById(R.id.description_addoffer_item);
            priceInput = itemView.findViewById(R.id.title_product_addoffer_item);
        }
    }

}
