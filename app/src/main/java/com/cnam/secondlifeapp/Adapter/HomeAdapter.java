package com.cnam.secondlifeapp.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.ui.offer.OfferFragment;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {

    private Context context;
    private List<Offer> homeList;


    public HomeAdapter(Context context, List<Offer> homeList) {
        this.context = context;
        this.homeList = homeList;
    }


    public Offer getHome(int position){ return homeList.get(position);
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.home_item, parent,false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        Offer offer = homeList.get(position);

        String title = offer.getTitle();

        holder.title.setText(title);

       Picasso.get().load(offer.getProducts().get(0).getPictureUrl()).into(holder.homePicture);

       holder.btnSee.setOnClickListener((View.OnClickListener) v -> {
           AppCompatActivity activity = (AppCompatActivity)v.getContext();
           FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
           OfferFragment offerFragment = new OfferFragment();

           Bundle bundle = new Bundle();
           bundle.putSerializable("offer", (Serializable) offer);
           offerFragment.setArguments(bundle);
           ft.replace(R.id.nav_host_fragment, offerFragment);
           ft.addToBackStack(null);
           ft.commit();
       });
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


    public static class HomeViewHolder extends RecyclerView.ViewHolder{
        TextView title, basePrice;
        ImageView homePicture;
        Button btnSee;


        public HomeViewHolder(View itemView){
            super(itemView);
            homePicture = itemView.findViewById(R.id.pictureProduct);
            title = itemView.findViewById(R.id.title_item);
            btnSee = itemView.findViewById(R.id.ButtonHome_Proposal);
        }
    }
}
