package com.cnam.secondlifeapp.Dao;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.cnam.secondlifeapp.model.Opinion;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

@Dao
public interface OpinionDao {
    @Query("SELECT * FROM opinion")
    LiveData<List<Opinion>> getAll();

    @Query("SELECT * FROM opinion WHERE id IN (:id)")
    LiveData<Opinion> findById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Opinion... opinion);

    @Update
    void update(Opinion opinion);

    @Delete
    void delete(Opinion opinion);

    @Query("DELETE FROM opinion")
    void deleteAll();
}
