package com.cnam.secondlifeapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cnam.secondlifeapp.network.JsonPlaceHolderApi;
import com.cnam.secondlifeapp.network.LoginRequest;
import com.cnam.secondlifeapp.network.LoginResponse;
import com.cnam.secondlifeapp.network.RetrofitService;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    TextInputLayout username;
    TextInputLayout password;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextView textViewPasswordForgotten = findViewById(R.id.TextViewPasswordForgotten);
        TextView textViewTextViewSignUp = findViewById(R.id.TextViewSignUp);
        Button button = findViewById(R.id.ButtonSignIn);
        password = findViewById(R.id.editTextTextPassword);
        username = findViewById(R.id.comment_item);

        // Boutton mot de passe oublié
        textViewPasswordForgotten.setOnClickListener(v -> {
            Intent intentPasswordForgotten = new Intent(this, ResetPasswordActivity.class);
            startActivity(intentPasswordForgotten);
        });

        // Boutton inscription
        textViewTextViewSignUp.setOnClickListener(a -> {
            Intent intentSignUp = new Intent(this, CreateProfileActivity.class);
            startActivity(intentSignUp);
        });

        new Thread(() -> {
            // Boutton se connecter
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TextUtils.isEmpty(username.getEditText().getText().toString()) || TextUtils.isEmpty(password.getEditText().getText().toString()))
                    {
                        Toast.makeText(LoginActivity.this,"Champs incomplets", Toast.LENGTH_LONG).show();
                    }
                    else {
                        connection();
                    }
                }
            });
        }).start();
    }

    private void connection() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(username.getEditText().getText().toString());
        loginRequest.setPassword(password.getEditText().getText().toString());


        Call<LoginResponse> loginResponseCall = RetrofitService.auth().loginUser(loginRequest);

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()){

                    LoginResponse registerResponse = response.body();
                    Intent intentAppHome = new Intent(LoginActivity.this, MainActivity.class);
                    intentAppHome.putExtra("Token", registerResponse.getToken());
                    startActivity(intentAppHome);

                }else{
                    Toast.makeText(LoginActivity.this,"Vos identifiants sont incorrects", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                //Toast.makeText(LoginActivity.this,"Throwable "+t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Toast.makeText(LoginActivity.this,"Problème de connexion", Toast.LENGTH_LONG).show();
            }
        });
    }
}
