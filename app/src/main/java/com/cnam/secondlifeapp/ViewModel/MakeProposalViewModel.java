package com.cnam.secondlifeapp.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MakeProposalViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public MakeProposalViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Make Proposal Fragment");
    }

    public LiveData<String> getText() {return mText;}
}