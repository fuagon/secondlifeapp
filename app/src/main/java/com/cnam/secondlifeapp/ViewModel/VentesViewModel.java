package com.cnam.secondlifeapp.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class VentesViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public VentesViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is ventes fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}