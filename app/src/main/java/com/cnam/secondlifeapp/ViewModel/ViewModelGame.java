package com.cnam.secondlifeapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.model.Game;
import com.cnam.secondlifeapp.repository.GameRepository;

import java.util.List;

public class ViewModelGame extends AndroidViewModel {
    private GameRepository gameRepository;
    private LiveData<List<Game>> allGame;

    public ViewModelGame(@NonNull Application application) {
        super(application);
        gameRepository = new GameRepository(application);
        allGame = gameRepository.getAllUsers();
    }


    public void insert(Game game){
        gameRepository.insert(game);
    }

    public void update(Game game){
        gameRepository.update(game);
    }

    public void delete(Game game){
        gameRepository.delete(game);
    }

    public void deleteAllUser(){
        gameRepository.deleteAll();
    }

    public LiveData<List<Game>> getAllGame(){
        return allGame;
    }
}
