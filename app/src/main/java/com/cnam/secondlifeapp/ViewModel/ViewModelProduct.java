package com.cnam.secondlifeapp.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.model.User;
import com.cnam.secondlifeapp.repository.ProductRepository;

import java.util.List;

public class ViewModelProduct extends AndroidViewModel {

    private ProductRepository productRepository;
    private LiveData<List<Product>> allProduct;

    public ViewModelProduct(@NonNull Application application) {
        super(application);
        productRepository = new ProductRepository(application);
        allProduct = productRepository.getAllProduct();
    }

    public void insert(Product product){
        productRepository.insert(product);
    }

    public void update(Product product){
        productRepository.update(product);
    }

    public void delete(Product product){
        productRepository.delete(product);
    }

    public void deleteAllUser(){
        productRepository.deleteAll();
    }

    public LiveData<List<Product>> getAllProduct(){
        return allProduct;
    }


}
