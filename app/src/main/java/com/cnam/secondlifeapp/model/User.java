package com.cnam.secondlifeapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "user")
public class User implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("mail")
    @ColumnInfo(name = "mail")
    private String mail;

    @SerializedName("username")
    @ColumnInfo(name = "username")
    private String username;

    @SerializedName("password")
    @ColumnInfo(name = "password")
    private String password;

    @SerializedName("phoneNumber")
    @ColumnInfo(name = "phoneNumber")
    private String phoneNumber;

    @SerializedName("street")
    @ColumnInfo(name = "street")
    private String street;


    @SerializedName("streetnumber")
    @ColumnInfo(name = "streetnumber")
    private String streetnumber;

    @SerializedName("city")
    @ColumnInfo(name = "city")
    private String city;

    @SerializedName("postalCode")
    @ColumnInfo(name = "postalCode")
    private String postalCode;

    public User(int id, String mail, String username, String password, String phoneNumber, String street, String streetnumber, String city, String postalCode) {
        this.id = id;
        this.mail = mail;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;

        this.street = street;
        this.streetnumber = streetnumber;
        this.city = city;
        this.postalCode = postalCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStreetnumber() {
        return streetnumber;
    }

    public void setStreetnumber(String streetnumber) {
        this.streetnumber = streetnumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }


}
