package com.cnam.secondlifeapp.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.Dao.GameDao;
import com.cnam.secondlifeapp.database.AppDatabase;
import com.cnam.secondlifeapp.model.Game;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

public class GameRepository {
    private final GameDao gameDao;
    private final LiveData<List<Game>> listLiveGameData;

    public GameRepository(Application application){
        AppDatabase db = AppDatabase.getDatabase(application);
        gameDao = db.gameDao();
        listLiveGameData = gameDao.getAll();
    }


    public LiveData<List<Game>> getAllUsers(){
        return listLiveGameData;
    }


    public void insert(Game game){
        new InsertGameAsyncTask(gameDao).execute(game);
    }


    public void update(Game game){
        new UpdateGameAsyncTask(gameDao).execute(game);
    }

    public void delete(Game game){
        new DeleteGameAsyncTask(gameDao).execute(game);
    }

    public void deleteAll(){
        new DeleteAllGameAsyncTask(gameDao).execute();
    }




    private static class InsertGameAsyncTask extends AsyncTask<Game, Void, Void> {
        private final GameDao gameDao;

        private InsertGameAsyncTask(GameDao gameDao) {
            this.gameDao = gameDao;
        }


        @Override
        protected Void doInBackground(Game... games) {
            gameDao.insert(games[0]);
            return null;
        }
    }
    private static class UpdateGameAsyncTask extends AsyncTask<Game, Void, Void> {
        private final GameDao gameDao;

        private UpdateGameAsyncTask(GameDao gameDao) {
            this.gameDao = gameDao;
        }


        @Override
        protected Void doInBackground(Game... games) {
            gameDao.update(games[0]);
            return null;
        }
    }
    private static class DeleteGameAsyncTask extends AsyncTask<Game, Void, Void> {
        private final GameDao gameDao;

        private DeleteGameAsyncTask(GameDao gameDao) {
            this.gameDao = gameDao;
        }


        @Override
        protected Void doInBackground(Game... games) {
            gameDao.delete(games[0]);
            return null;
        }
    }
    private static class DeleteAllGameAsyncTask extends AsyncTask<Void, Void, Void> {
        private final GameDao gameDao;

        private DeleteAllGameAsyncTask(GameDao gameDao) {
            this.gameDao = gameDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            gameDao.deleteAll();
            return null;
        }
    }

}
