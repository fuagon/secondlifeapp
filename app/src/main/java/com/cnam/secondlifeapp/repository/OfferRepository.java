package com.cnam.secondlifeapp.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cnam.secondlifeapp.Dao.OffersDao;
import com.cnam.secondlifeapp.database.AppDatabase;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.User;

import java.util.List;

public class OfferRepository {
    private final OffersDao offerDao;
    private final LiveData<List<Offer>> listLiveOfferData;

    public OfferRepository(Application application){
        AppDatabase db = AppDatabase.getDatabase(application);
        offerDao = db.offersDao();
        listLiveOfferData = offerDao.getAll();
    }
    public LiveData<List<Offer>> getAllOffer() { return listLiveOfferData;}

    public void insert(Offer offer){
        new InsertOfferAsyncTask(offerDao).execute(offer);
    }


    public void update(Offer offer){
        new UpdateOfferAsyncTask(offerDao).execute(offer);
    }

    public void delete(Offer offer){
        new DeleteOfferAsyncTask(offerDao).execute(offer);
    }

    public void deleteAll(){

        new DeleteAllOfferAsyncTask(offerDao).execute();
    }



    private static class InsertOfferAsyncTask extends AsyncTask<Offer, Void, Void> {
        private final OffersDao offerDao;

        private InsertOfferAsyncTask(OffersDao offerDao) {
            this.offerDao = offerDao;
        }


        @Override
        protected Void doInBackground(Offer... offer) {
            offerDao.insert(offer[0]);
            return null;
        }
    }
    private static class UpdateOfferAsyncTask extends AsyncTask<Offer, Void, Void> {
        private final OffersDao offerDao;

        private UpdateOfferAsyncTask(OffersDao offerDao) {
            this.offerDao = offerDao;
        }


        @Override
        protected Void doInBackground(Offer... offer) {
            offerDao.update(offer[0]);
            return null;
        }
    }
    private static class DeleteOfferAsyncTask extends AsyncTask<Offer, Void, Void> {
        private final OffersDao offerDao;

        private DeleteOfferAsyncTask(OffersDao offerDao) {
            this.offerDao = offerDao;
        }


        @Override
        protected Void doInBackground(Offer... offer) {
            offerDao.delete(offer[0]);
            return null;
        }
    }
    private static class DeleteAllOfferAsyncTask extends AsyncTask<Void, Void, Void> {
        private final OffersDao offerDao;

        private DeleteAllOfferAsyncTask(OffersDao offerDao) {
            this.offerDao = offerDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            offerDao.deleteAll();
            return null;
        }
    }


}
