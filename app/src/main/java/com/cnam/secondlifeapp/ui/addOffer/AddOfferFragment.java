package com.cnam.secondlifeapp.ui.addOffer;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.Adapter.AddOfferAdapter;
import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.ViewModel.AddOfferViewModel;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.databinding.FragmentAddofferBinding;
import com.cnam.secondlifeapp.model.Game;
import com.cnam.secondlifeapp.ui.home.HomeFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.cnam.secondlifeapp.network.RetrofitService;

import com.cnam.secondlifeapp.model.Offer;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddOfferFragment extends Fragment {

    private AddOfferViewModel dashboardViewModel;

    private static List<Product> productsList = new ArrayList<>();
    private List<Game> gameList = new ArrayList<>();
    Button buttonAddOffer, buttonAddProduct;
    TextInputEditText title_offer,price_offer,description_offer, description_product, title_product;
    AutoCompleteTextView searchListGame;
    Game gameSelected;
    private Offer offers = new Offer();
    String titleProduct, descriptionProduct;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel = new ViewModelProvider(this).get(AddOfferViewModel.class);
        com.cnam.secondlifeapp.databinding.FragmentAddofferBinding fragmentAddOfferBinding = FragmentAddofferBinding.inflate(inflater, container, false);
        View root = fragmentAddOfferBinding.getRoot();

        RecyclerView.LayoutManager myLayoutManager = new LinearLayoutManager(getContext());
        AddOfferAdapter addOfferAdapter = new AddOfferAdapter(productsList);
        buttonAddProduct = fragmentAddOfferBinding.ButtonAddProduct;
        buttonAddOffer = fragmentAddOfferBinding.ButtonAddOffer;
        title_offer = fragmentAddOfferBinding.titleOffer;
        searchListGame = fragmentAddOfferBinding.searchListGame;
        price_offer = fragmentAddOfferBinding.priceAddofferItem;
        description_offer = fragmentAddOfferBinding.descriptionOfferAddofferItem;
        description_product = fragmentAddOfferBinding.descriptionAddofferItem;
        title_product = fragmentAddOfferBinding.titleProductAddofferItem;

        setHasOptionsMenu(true);

        // instanciation de la liste déroulante
        getGames();

        ArrayAdapter<Game> adapter = new ArrayAdapter<Game>(getContext(),android.R.layout.simple_list_item_1, gameList);
        searchListGame.setAdapter(adapter);

        searchListGame.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                gameSelected = gameList.get(position);
            }
        });

        buttonAddOffer.setOnClickListener(v -> {

            if(!title_offer.getText().toString().equals("") && !description_offer.getText().toString().equals("") && !price_offer.getText().toString().equals("")) {
                offers.setTitle(title_offer.getText().toString());
                offers.setDescription(description_offer.getText().toString());
                offers.setBasePrice(Float.parseFloat(price_offer.getText().toString()));
                offers.setProducts(productsList);
            }else{
                Toast.makeText(getContext(), "Veuillez renseigner les champs vides", Toast.LENGTH_SHORT).show();
            }
            if(!productsList.isEmpty()) {
                sendOffer(offers);
            }else{
                Toast.makeText(getContext(), "Veuillez renseigner un jeu", Toast.LENGTH_SHORT).show();
            }
        });


        buttonAddProduct.setOnClickListener(v ->
        {

            if (!title_product.getText().toString().equals("") && !description_product.getText().toString().equals("") && gameSelected != null) {

                Product products = new Product();
                products.setName(title_product.getText().toString());
                products.setDescription(description_product.getText().toString());
                products.setPictureUrl(gameSelected.getPictureUrl());
                products.setGame(gameSelected);
                productsList.add(products);
            }
            else
                {
                Toast.makeText(getContext(), "Veuillez renseigner un jeu", Toast.LENGTH_SHORT).show();
            }

            title_product.setText("");
            description_product.setText("");
            searchListGame.setText("");
            Toast.makeText(getContext(), "Ajoutez un nouveau jeu", Toast.LENGTH_SHORT).show();

        });

        return root;
    }

    public void getGames(){

        Call<List<Game>> getGamesCall = RetrofitService.auth().getGames();

        getGamesCall.enqueue(new Callback<List<Game>>() {
            @Override
            public void onResponse(Call<List<Game>> call, Response<List<Game>> response) {

                List<Game> games = response.body();
                if(games != null){
                    gameList.clear();
                    gameList.addAll(games);
                }
            }
            @Override
            public void onFailure(Call<List<Game>> call, Throwable t) {
                // textViewResult.setText(t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public int getSelectedGame(View v){
        int game =  searchListGame.getListSelection();
        return game;
    }

    public void sendOffer(Offer offers) {

        MainActivity mainActivity = (MainActivity) getActivity();
        String token = mainActivity.getToken();

        Call<Offer> getGamesCall = RetrofitService.auth().createOffer("Bearer " + token, offers);
        getGamesCall.enqueue(new Callback<Offer>() {
            @Override
            public void onResponse(Call<Offer> call, Response<Offer> response) {
                productsList.clear();
                AppCompatActivity activity = (AppCompatActivity) getContext();
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                HomeFragment homeFragment = new HomeFragment();
                ft.replace(R.id.nav_host_fragment, homeFragment);
                ft.addToBackStack(null);
                ft.commit();
            }
            @Override
            public void onFailure(Call<Offer> call, Throwable t) {
                Toast.makeText(mainActivity.getApplicationContext(), "Problème de connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

}