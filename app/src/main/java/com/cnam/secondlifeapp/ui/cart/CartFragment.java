package com.cnam.secondlifeapp.ui.cart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.Adapter.PurchaseAdapter;
import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.ViewModel.CartViewModel;
import com.cnam.secondlifeapp.databinding.FragmentCartBinding;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.network.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartFragment extends Fragment {

    private FragmentCartBinding fragmentCartBinding;
    private RecyclerView purchaseRecycler;
    private static PurchaseAdapter purchaseAdapter;
    private RecyclerView.LayoutManager myLayoutManager;
    private static List<Offer> purchaseList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        CartViewModel cartViewModel = new ViewModelProvider(this).get(CartViewModel.class);
        View root = inflater.inflate(R.layout.fragment_cart, container, false);

        purchaseRecycler = root.findViewById(R.id.purchase_recycler);
        myLayoutManager = new LinearLayoutManager(getContext());

        purchaseRecycler.setLayoutManager(myLayoutManager);

        purchaseAdapter = new PurchaseAdapter(purchaseList);
        purchaseRecycler.setAdapter(purchaseAdapter);

        getMyPurchase();

        setHasOptionsMenu(true);
        return root;
    }

    private void getMyPurchase() {
        MainActivity mainActivity = (MainActivity) getActivity();
        String token = mainActivity.getToken();

        Call<List<Offer>> listCall = RetrofitService.auth().getListPurchase("Bearer "+ token);

        listCall.enqueue(new Callback<List<Offer>>() {
            @Override
            public void onResponse(Call<List<Offer>> call, Response<List<Offer>> response) {
                List<Offer> offer = response.body();

                if(offer != null){
                    purchaseList.clear();
                    purchaseList.addAll(offer);
                    purchaseAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Offer>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}