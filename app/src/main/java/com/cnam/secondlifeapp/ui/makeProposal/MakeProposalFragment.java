package com.cnam.secondlifeapp.ui.makeProposal;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.ViewModel.MakeProposalViewModel;
import com.cnam.secondlifeapp.databinding.FragmentMakeproposalBinding;
import com.cnam.secondlifeapp.model.Game;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.Product;
import com.cnam.secondlifeapp.model.Proposal;
import com.cnam.secondlifeapp.network.RetrofitService;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MakeProposalFragment extends Fragment {

    private TextView title_makeproposal_item;
    private ImageView picture_makeproposal_item;
    private TextInputLayout makeProposalText;

    private Button buttonMakeProposal, buttonAddProduct;
    private List<Game> gameList = new ArrayList<>();
    private static List<Product> productsList = new ArrayList<>();
    private AutoCompleteTextView searchListGame;
    private Game gameSelected;
    private Proposal proposal = new Proposal();

    private FragmentMakeproposalBinding fragmentMakeproposalBinding;

     public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MakeProposalViewModel makeProposalViewModel = new ViewModelProvider(this).get(MakeProposalViewModel.class);
        Bundle bundle = getArguments();
        Offer offers = (Offer) bundle.getSerializable("offer1");
        fragmentMakeproposalBinding = FragmentMakeproposalBinding.inflate(inflater, container, false);
        title_makeproposal_item = fragmentMakeproposalBinding.titleMakeproposalItem;
        picture_makeproposal_item = fragmentMakeproposalBinding.pictureMakeproposalItem;
        makeProposalText = fragmentMakeproposalBinding.makeProposalText;
        searchListGame = fragmentMakeproposalBinding.searchListGameProposal;
        buttonMakeProposal = fragmentMakeproposalBinding.ButtonMakeProposal;
        buttonAddProduct = fragmentMakeproposalBinding.ButtonAddProposal;
         TextInputLayout title_product = fragmentMakeproposalBinding.makeProposalText2;
         TextInputLayout description_product = fragmentMakeproposalBinding.makeProposalText3;
        title_makeproposal_item.setText(offers.getTitle());

        Picasso.get().load(offers.getProducts().get(0).getPictureUrl()).resize(500,500).into(picture_makeproposal_item);

        getGames();

         ArrayAdapter<Game> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, gameList);
         searchListGame.setAdapter(adapter);

         searchListGame.setOnItemClickListener((arg0, arg1, position, arg3) -> gameSelected = gameList.get(position));



        buttonAddProduct.setOnClickListener(v -> {

            Product products = new Product();
            products.setName(title_product.getEditText().getText().toString());
            products.setDescription(description_product.getEditText().getText().toString());
            products.setPictureUrl(gameSelected.getPictureUrl());
            products.setGame(gameSelected);
            productsList.add(products);
            searchListGame.setText("");
            Toast.makeText(getContext(), "Ajoutez un nouveau jeu", Toast.LENGTH_SHORT).show();
        });

         buttonMakeProposal.setOnClickListener(v -> {

             Proposal proposal = new Proposal(offers,productsList,Float.parseFloat(makeProposalText.getEditText().getText().toString()));
             sendProposal(proposal);
             Log.d("ok ", "Fait");
         });

        return fragmentMakeproposalBinding.getRoot();

    }

        public void sendProposal(Proposal proposal){
            MainActivity mainActivity = (MainActivity) getActivity();
            String token = mainActivity.getToken();

            Call<Proposal> proposalCall = RetrofitService.auth().createProposal("Bearer "+ token,proposal);

            proposalCall.enqueue(new Callback<Proposal>() {
                @Override
                public void onResponse(Call<Proposal> call, Response<Proposal> response) {
                    Proposal proposal = response.body();
                }

                @Override
                public void onFailure(Call<Proposal> call, Throwable t) {

                }
            });
        }

    public void getGames(){

        Call<List<Game>> getGamesCall = RetrofitService.auth().getGames();

        getGamesCall.enqueue(new Callback<List<Game>>() {
            @Override
            public void onResponse(Call<List<Game>> call, Response<List<Game>> response) {

                List<Game> games = response.body();
                if(games != null){
                    gameList.clear();
                    gameList.addAll(games);
                }
            }
            @Override
            public void onFailure(Call<List<Game>> call, Throwable t) {
                // textViewResult.setText(t.getMessage());
                t.printStackTrace();
            }
        });
    }


}
