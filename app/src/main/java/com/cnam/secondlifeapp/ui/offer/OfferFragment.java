package com.cnam.secondlifeapp.ui.offer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cnam.secondlifeapp.Adapter.OfferAdapter;
import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.ViewModel.OfferViewModel;
import com.cnam.secondlifeapp.databinding.FragmentOfferBinding;
import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.ui.makeProposal.MakeProposalFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class OfferFragment extends Fragment {

    private TextView username_seller;
    private ImageView offer_game;
    private Button btnMakeOffer;
    private OfferAdapter offersAdapter;
    private static List<Offer> offersList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        OfferViewModel offerViewModel = new ViewModelProvider(this).get(OfferViewModel.class);
        FragmentOfferBinding fragmentOfferBinding = FragmentOfferBinding.inflate(inflater, container, false);
        View view = fragmentOfferBinding.getRoot();

        Bundle bundle = getArguments();
        Offer offer = (Offer) bundle.getSerializable("offer");

        RecyclerView horizontalRecycler = view.findViewById(R.id.product_detail_recycler);
        RecyclerView.LayoutManager myLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false);

        horizontalRecycler.setLayoutManager(myLayoutManager);

        offersAdapter = new OfferAdapter(getContext(), offer);
        horizontalRecycler.setAdapter(offersAdapter);


       TextView offer_name = fragmentOfferBinding.offerName;
       TextView price = fragmentOfferBinding.price;
       TextView description = fragmentOfferBinding.description;
       btnMakeOffer = fragmentOfferBinding.ButtonMakeOffer;
       offer_name.setText(offer.getTitle());
       price.setText(String.format(" %s €", offer.getBasePrice()));
       description.setText(offer.getDescription());



        btnMakeOffer.setVisibility(View.VISIBLE);
        btnMakeOffer.setOnClickListener(v -> {
            AppCompatActivity activity = (AppCompatActivity)v.getContext();
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            MakeProposalFragment makeProposalFragment = new MakeProposalFragment();

            Bundle bundle1 = new Bundle();
            bundle1.putSerializable("offer1", (Serializable) offer);
            makeProposalFragment.setArguments(bundle1);
            ft.replace(R.id.fragment_offer , makeProposalFragment);
            ft.addToBackStack(null);
            ft.commit();
            btnMakeOffer.setVisibility(View.INVISIBLE);
        });

        setHasOptionsMenu(true);
        return view;
    }

}
