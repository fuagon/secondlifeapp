package com.cnam.secondlifeapp.ui.profil;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.cnam.secondlifeapp.LoginActivity;
import com.cnam.secondlifeapp.MainActivity;
import com.cnam.secondlifeapp.R;
import com.cnam.secondlifeapp.ViewModel.ProfilViewModel;
import com.cnam.secondlifeapp.databinding.FragmentProfilBinding;

import com.cnam.secondlifeapp.model.Offer;
import com.cnam.secondlifeapp.model.User;
import com.cnam.secondlifeapp.network.ProfileModifRequest;
import com.cnam.secondlifeapp.network.ProfileResponse;
import com.cnam.secondlifeapp.network.RetrofitService;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilFragment extends Fragment {

    private ProfilViewModel profilViewModel;
    private User user;

    TextInputLayout editTextEmailAddress,editTextPseudo,editTextPassword,editTextConfirmPassword,editTextPhoneNumber,editTextStreetNumber,editTextStreet,editTextCity,editTextPostalCode;
    String id,mail,username,password,phonenumber,streetnumber,street,city,postalcode;
    Button buttonSubmitProfile;

    private FragmentProfilBinding fragmentProfilBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        profilViewModel = new ViewModelProvider(this).get(ProfilViewModel.class);
        fragmentProfilBinding = FragmentProfilBinding.inflate(inflater, container, false);
        View root = fragmentProfilBinding.getRoot();

        editTextEmailAddress = fragmentProfilBinding.editTextEmailAddress;
        editTextPseudo = fragmentProfilBinding.editTextPseudo;
        editTextPassword = fragmentProfilBinding.editTextPassword;
        editTextConfirmPassword = fragmentProfilBinding.editTextConfirmPassword;
        editTextPhoneNumber = fragmentProfilBinding.editTextPhoneNumber;
        editTextStreetNumber = fragmentProfilBinding.editTextStreetNumber;
        editTextStreet = fragmentProfilBinding.editTextStreet;
        editTextCity = fragmentProfilBinding.editTextCity;
        editTextPostalCode = fragmentProfilBinding.editTextPostalCode;
        buttonSubmitProfile = fragmentProfilBinding.buttonSubmitProfile;

        getUserInfo();

        buttonSubmitProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sendMail = editTextEmailAddress.getEditText().getText().toString();
                String sendUsername = editTextPseudo.getEditText().getText().toString();
                String sendPassword = editTextPassword.getEditText().getText().toString();
                String sendPassword2 = editTextConfirmPassword.getEditText().getText().toString();
                String sendPhoneNumber = editTextPhoneNumber.getEditText().getText().toString();
                String sendStreetNumber = editTextStreetNumber.getEditText().getText().toString();
                String sendStreet = editTextStreet.getEditText().getText().toString();
                String sendCity = editTextCity.getEditText().getText().toString();
                String sendPostalCode = editTextPostalCode.getEditText().getText().toString();

                if (sendPassword2.equals(sendPassword)){

                    MainActivity mainActivity = (MainActivity) getActivity();
                    String token = mainActivity.getToken();

                    user.setId(Integer.parseInt(id));
                    user.setCity(sendCity);
                    user.setMail(sendMail);
                    user.setPassword(sendPassword);
                    user.setPhoneNumber(sendPhoneNumber);
                    user.setPostalCode(sendPostalCode);
                    user.setStreet(sendStreet);
                    user.setStreetnumber(sendStreetNumber);
                    user.setUsername(sendUsername);

                    Call<Void> profileModifRequestCall = RetrofitService.auth().profilModif("Bearer "+token, user);

                    profileModifRequestCall.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if(response.isSuccessful()){

                                Log.d("ok ", "modif successful (info user)");
                                Intent intent = new Intent(getContext(), LoginActivity.class);
                                startActivity(intent);

                            }else{
                                Log.d("Erreur ","modif not successful (info user)");
                                Toast.makeText(mainActivity.getApplicationContext(), "Problème avec les informations renseignées", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            //Toast.makeText(mainActivity.getApplicationContext(),"Throwable "+t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            Toast.makeText(mainActivity.getApplicationContext(), "Problème de connexion", Toast.LENGTH_SHORT).show();
                            Log.d("Erreur ","Problème avec l'API");
                        }
                    });
                }
            }
        });

        return root;
    }

    public void getUserInfo() {
        MainActivity mainActivity = (MainActivity) getActivity();
        String token = mainActivity.getToken();

        Call<ProfileResponse> profileResponseCall = RetrofitService.auth().profilInfo("Bearer "+token);
        profileResponseCall.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if(response.isSuccessful()){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            id = response.body().getId();
                            mail = response.body().getMail();
                            username = response.body().getUsername();
                            password = response.body().getPassword();
                            phonenumber = response.body().getPhonenumber();
                            streetnumber = response.body().getStreetnumber();
                            street = response.body().getStreet();
                            city = response.body().getCity();
                            postalcode = response.body().getPostalcode();

                            editTextEmailAddress.getEditText().setText(mail);
                            editTextPseudo.getEditText().setText(username);
                            editTextPassword.getEditText().setText(password);
                            editTextConfirmPassword.getEditText().setText(password);
                            editTextPhoneNumber.getEditText().setText(phonenumber);
                            editTextStreetNumber.getEditText().setText(streetnumber);
                            editTextStreet.getEditText().setText(street);
                            editTextCity.getEditText().setText(city);
                            editTextPostalCode.getEditText().setText(postalcode);
                        }
                    },700);
                }else{
                    Toast.makeText(mainActivity.getApplicationContext(), "Problème avec les informations renseignées", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                Toast.makeText(mainActivity.getApplicationContext(), "Problème de connection", Toast.LENGTH_SHORT).show();
            }
        });
    }
}